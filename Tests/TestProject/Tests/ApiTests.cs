using System;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using TestingCore.Implementations.ApiFoundation.Models;
using TestProject.TestBases;

namespace TestProject.Tests
{
    [TestFixture(Category = "API")]
    public class Tests : ApiTestBase
    {
        [TestCase(12, 2, 6, 1)]
        [TestCase(12, 2, 6, 2)]
        public async Task GetUserListAsyncTest(int expectedTotalUsersCount,
            int expectedTotalPagesCount,
            int expectedUsersOnPageCount,
            int page)
        {
            var response = await apiClient.GetUserListAsync(page);
            var result = response.Value;

            result.PageNumber.Should().Be(page,
                BuildErrorMessage("вернется страница", page, result.PageNumber));
            result.TotalPagesCount.Should().Be(expectedTotalPagesCount,
                BuildErrorMessage("всего страниц", expectedTotalPagesCount, result.TotalPagesCount));
            result.TotalUsersCount.Should().Be(expectedTotalUsersCount,
                BuildErrorMessage("общее число юзеров", expectedTotalUsersCount, result.TotalUsersCount));
            result.UsersOnPageCount.Should().Be(expectedUsersOnPageCount,
                BuildErrorMessage("пользователей на странице", expectedUsersOnPageCount, result.UsersOnPageCount));
        }

        [TestCase]
        public async Task CreateUserAsyncTest()
        {
            var currentDate = DateTime.Now.AddMilliseconds(-1);
            var model = new ShortUserModel
            {
                Name = "morpheus",
                Job = "leader"
            };

            var response = await apiClient.CreateUserAsync(model).ConfigureAwait(false);
            var result = response.Value;
            DateTime.TryParse(result.CreatedDate, out var updateAtDateResult);

            result.Name.Should().Be(model.Name);
            result.Job.Should().Be(model.Job);
            currentDate.Should().BeBefore(updateAtDateResult);
        }

        private string BuildErrorMessage(string nameOfExpectedResource, int expectedResult, int result)
        {
            return $"Ожидали, что {nameOfExpectedResource} - '{expectedResult}', но в ответе {result}";
        }
    }
}