using NUnit.Framework;

namespace TestProject.TestBases
{
    [SetUpFixture]
    public class AssemblyTestBase
    {
        /// <summary>
        /// Метод запустится перед запуском всех тестов. Конфигурируем тут.
        /// </summary>
        [OneTimeSetUp]
        protected void BeforeAllTestsSetUp()
        {
        }
    }
}