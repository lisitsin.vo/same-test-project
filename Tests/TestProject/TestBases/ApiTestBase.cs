using TestingCore.Implementations.ApiFoundation.Factories;
using TestingCore.Implementations.ApiFoundation.Interfaces;
using TestingCore.Implementations.NetworkCommands;
using TestingCore.TestBases;

namespace TestProject.TestBases
{
    public class ApiTestBase : TestBase
    {
        protected IApiClient apiClient;

        protected override void FixtureSetUp()
        {
            base.FixtureSetUp();

            base.SetUp();
            var serializer = new Serializer();
            apiClient = new ApiClientFactory(serializer).Create();
        }
    }
}