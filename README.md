Тестовый фреймворк для работы с АПИ, тесты на API

Минимальный уровень работы с тестами на этапе разработки и передачи в тестирование.
Итак, у нас имеется несколько типов задач, связанных с написанием кода:

Новая фича.	Покрывается тестами юнит-тестами, интеграционными. Основной сценарий в виде end-to-end
Эксперимент / подпольная задача.	Накидываются только сценарии, по факту эксперимента, в случае его успешности тесты попадают в технический долг. Пишутся юнит-тесты
Баттл/багфикс/хотфикс	Пишется тест закрывающий сценарий, который был поправлен. Хотфиксов в идеале быть не должно, откатываем проблемные релизы


Перед сдачей задачи в тестирование необходимо чтобы были выполнены следующие пункты:
(отметить) Ревью должно быть пройдено.
Почему: после прохождения ревью задача может быть частично/полностью переписана.

AsIs: задачи попадают в тестирование до/в процессе ревью, порой приходится перетестировать задачу полностью.

(отметить) Тесты на все публичные методы сервисов должны быть написаны, старые тесты должны быть причесаны (вынесены тесты на нашу логику в отдельные сьюты с понятным названием, и т.п.). При ревью задачи необходимо уделять внимание покрытию, качеству тестов.
Почему: мы должны понимать, что уже покрыто, что необходимо покрыть.

AsIs: названия тестов, сьюты не отражают что они проверяют, все свалено в кучу.

(отметить) Тесты в CI должны быть прогнаны, проанализированы, подняты.
Почему: в этом и смысл CI.

AsIs: задачи попадают в тестирование без запуска автотестов в TC (практически половина по ощущениям), помимо тех, которые гоняются автоматом перед деплоем.

(отметить) Перед раскатыванием на стенд необходимо подлить в ветку релиз, при первой перевыкладке деплоить все сервисы
Почему: бывают конфликты.

AsIs: в части задач нам приходится еще раз разливать весь стенд со свежим релизом.