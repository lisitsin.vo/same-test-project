using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace TestingCore.Implementations.Extensions
{
    public static class StreamExtensions
    {
        private static readonly Encoding utf8 = new UTF8Encoding(false);
        private static readonly byte[] newLineBytes = Encoding.ASCII.GetBytes(Environment.NewLine);

        public static Stream ToStream(this byte[] bytes)
        {
            return new MemoryStream(bytes, 0, bytes.Length, false, true);
        }

        public static byte[] ReadToEnd(this Stream stream)
        {
            byte[] buffer = new byte[1024];
            MemoryStream memoryStream = new MemoryStream();
            int count;
            do
            {
                count = stream.Read(buffer, 0, 1024);
                memoryStream.Write(buffer, 0, count);
            } while (count > 0);

            return memoryStream.ToArray();
        }

        public static async Task<byte[]> ReadToEndAsync(this Stream stream)
        {
            byte[] buffer = new byte[1024];
            MemoryStream result = new MemoryStream();
            int size;
            do
            {
                size = await stream.ReadAsync(buffer, 0, 1024).ConfigureAwait(false);
                await result.WriteAsync(buffer, 0, size).ConfigureAwait(false);
            } while (size > 0);

            return result.ToArray();
        }

        public static void WriteLine(this Stream stream, string str, Encoding encoding)
        {
            stream.WriteBytes(encoding.GetBytes(str + "\r\n"));
        }

        public static void WriteASCIILine(this Stream stream, string str)
        {
            stream.WriteLine(str, Encoding.ASCII);
        }

        public static void WriteBytes(this Stream stream, byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0)
                return;
            stream.Write(bytes, 0, bytes.Length);
        }

        public static async Task WriteBytesAsync(this Stream stream, byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0)
                return;
            await stream.WriteAsync(bytes, 0, bytes.Length).ConfigureAwait(false);
        }

        public static void WriteString(this Stream stream, string str, Encoding encoding)
        {
            stream.WriteBytes(encoding.GetBytes(str));
        }

        public static void WriteUTF8String(this Stream stream, string str)
        {
            stream.WriteString(str, utf8);
        }

        public static byte[] ReadLine(this Stream stream)
        {
            MemoryStream memoryStream = new MemoryStream();
            int num1 = stream.ReadByte();
            if (num1 == -1)
                return null;
            do
            {
                int num2 = num1;
                num1 = stream.ReadByte();
                if (num2 != 13 || num1 != 10)
                    memoryStream.WriteByte((byte) num2);
                else
                    break;
            } while (num1 >= 0);

            return memoryStream.ToArray();
        }

        public static string ReadUTF8String(this Stream stream)
        {
            return stream.ReadString(utf8);
        }

        public static string ReadString(this Stream stream, Encoding encoding)
        {
            return encoding.GetString(stream.ReadToEnd());
        }

        public static byte[] ReadBytes(this Stream stream, int dataSize)
        {
            byte[] buffer = new byte[dataSize];
            int num = stream.Read(buffer, 0, dataSize);
            if (num != dataSize)
                throw new Exception(string.Format("Unexpected stream end. expected read {0} bytes, but was {1} bytes",
                    dataSize, num));
            return buffer;
        }

        public static void WriteEndOfLine(this Stream stream)
        {
            stream.WriteBytes(newLineBytes);
        }

        public static int GetNewLineBytesLength()
        {
            return newLineBytes.Length;
        }

        public static long CopyTo(this Stream source, Stream target, int bufferSize, long bytesToCopy)
        {
            return source.Split(bufferSize, bytesToCopy,
                (bytes, count) => target.Write(bytes, 0, count));
        }

        public static long CopyTo(this Stream source, Stream target, long bytesToCopy)
        {
            int bufferSize = bytesToCopy >= 8192L ? (bytesToCopy < 1048576L ? 8192 : 1048576) : (int) bytesToCopy;
            return source.CopyTo(target, bufferSize, bytesToCopy);
        }

        public static long Split(
            this Stream source,
            int bufferSize,
            long bytesToCopy,
            Action<byte[], int> action)
        {
            byte[] buffer = new byte[bufferSize];
            return Split(source, bytesToCopy, buffer, action);
        }

        private static long Split(
            Stream source,
            long bytesToCopy,
            byte[] buffer,
            Action<byte[], int> action)
        {
            long val1 = bytesToCopy;
            int num;
            while ((num = source.Read(buffer, 0, (int) Math.Min(val1, buffer.Length))) > 0)
            {
                val1 -= num;
                action(buffer, num);
            }

            return bytesToCopy - val1;
        }

        private static long CopyPrefixTo(
            this Stream source,
            Stream target,
            int bufferSize,
            long bytesToCopy)
        {
            return source.Split(bufferSize, bytesToCopy,
                (bytes, count) => target.Write(bytes, 0, count));
        }

        public static long CopyPrefixTo(this Stream source, Stream target, long bytesToCopy)
        {
            int bufferSize = bytesToCopy >= 8192L ? (bytesToCopy < 1048576L ? 8192 : 1048576) : (int) bytesToCopy;
            return source.CopyPrefixTo(target, bufferSize, bytesToCopy);
        }

        public static void WriteASCIIString(this Stream stream, string str)
        {
            stream.WriteString(str, Encoding.ASCII);
        }
    }
}