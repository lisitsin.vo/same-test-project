using TestingCore.Implementations.ApiFoundation.Clients;
using TestingCore.Implementations.ApiFoundation.Interfaces;
using TestingCore.Implementations.Extensions;
using TestingCore.Implementations.NetworkCommands;
using TestingCore.Implementations.NetworkCommands.Interfaces;

namespace TestingCore.Implementations.ApiFoundation.Factories
{
    public class ApiClientFactory : IApiClientFactory
    {
        private ISerializer serializer;
        private const string defaultHost = "https://reqres.in";

        public ApiClientFactory(ISerializer serializer)
        {
            this.serializer = serializer;
        }

        public IApiClient Create(string authToken = null,
                                 string authTokenHeader = null,
                                 string serverHost = defaultHost)
        {
            var usingHost = serverHost.IsNullOrEmpty() ? defaultHost : serverHost;
            var networkCommandFactory = new NetworkCommandFactory(usingHost, authToken, authTokenHeader);
            return new ApiClient(serializer, networkCommandFactory);
        }
    }
}