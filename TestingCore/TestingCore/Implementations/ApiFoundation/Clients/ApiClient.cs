using System.Net;
using System.Threading.Tasks;
using TestingCore.Implementations.ApiFoundation.Interfaces;
using TestingCore.Implementations.ApiFoundation.Models;
using TestingCore.Implementations.NetworkCommands;
using TestingCore.Implementations.NetworkCommands.Interfaces;

namespace TestingCore.Implementations.ApiFoundation.Clients
{
    public class ApiClient : IApiClient
    {
        private const int requestTimeoutInMills = 60000;
        private readonly ISerializer serializer;
        private readonly INetworkCommandFactory networkCommandFactory;

        public ApiClient(ISerializer serializer, INetworkCommandFactory networkCommandFactory)
        {
            this.serializer = serializer;
            this.networkCommandFactory = networkCommandFactory;
        }

        public async Task<Result<UsersPage>> GetUserListAsync(int page)
        {
            var response = await networkCommandFactory
                .CreateGet($"api/users", serializer.Accept)
                .AddQueryParameter("page", page)
                .SetTimeout(requestTimeoutInMills)
                .Build()
                .SendAsync().ConfigureAwait(false);

            return response.HttpStatusCode == HttpStatusCode.OK
                ? Result<UsersPage>.Ok(response.HttpStatusCode, serializer.Deserialize<UsersPage>(response.Value))
                : Result<UsersPage>.Failure(response.HttpStatusCode, response.ErrorMessage);
        }

        public async Task<Result<UserModel>> CreateUserAsync(ShortUserModel user)
        {
            var response = await networkCommandFactory
                .CreatePost($"api/users", serializer.Accept)
                .SetBody(serializer.Serialize(user), serializer.ContentType)
                .SetTimeout(requestTimeoutInMills)
                .SetSuccessHttpStatusCode(HttpStatusCode.Created)
                .Build()
                .SendAsync().ConfigureAwait(false);

            return response.HttpStatusCode == HttpStatusCode.Created
                ? Result<UserModel>.Ok(response.HttpStatusCode, serializer.Deserialize<UserModel>(response.Value))
                : Result<UserModel>.Failure(response.HttpStatusCode, response.ErrorMessage);
        }
    }
}