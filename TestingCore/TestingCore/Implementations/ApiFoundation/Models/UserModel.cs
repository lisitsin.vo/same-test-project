using Newtonsoft.Json;

namespace TestingCore.Implementations.ApiFoundation.Models
{
    public class UserModel
    {
        [JsonProperty("id")] public string Id { get; set; }
        [JsonProperty("name")] public string Name { get; set; }
        [JsonProperty("job")] public string Job { get; set; }
        [JsonProperty("createdAt")] public string CreatedDate { get; set; }
    }

    public class ShortUserModel
    {
        [JsonProperty("name")] public string Name { get; set; }
        [JsonProperty("job")] public string Job { get; set; }
    }
}