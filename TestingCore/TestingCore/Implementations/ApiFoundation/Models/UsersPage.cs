using Newtonsoft.Json;

namespace TestingCore.Implementations.ApiFoundation.Models
{
    public class UsersPage
    {
        [JsonProperty("page")] public int PageNumber { get; set; }
        [JsonProperty("per_page")] public int UsersOnPageCount { get; set; }
        [JsonProperty("total")] public int TotalUsersCount { get; set; }
        [JsonProperty("total_pages")] public int TotalPagesCount { get; set; }
        [JsonProperty("data")] public UserExtendedModel[] UsersData { get; set; }
    }
}