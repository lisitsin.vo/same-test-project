namespace TestingCore.Implementations.ApiFoundation.Interfaces
{
    public interface IApiClientFactory
    {
        IApiClient Create(string authToken = null, string authTokenHeader = null, string serverHost = null);
    }
}