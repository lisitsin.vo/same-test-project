using System.Threading.Tasks;
using TestingCore.Implementations.ApiFoundation.Models;
using TestingCore.Implementations.NetworkCommands;

namespace TestingCore.Implementations.ApiFoundation.Interfaces
{
    public interface IApiClient
    {
        Task<Result<UsersPage>> GetUserListAsync(int page);
        Task<Result<UserModel>> CreateUserAsync(ShortUserModel user);
    }
}