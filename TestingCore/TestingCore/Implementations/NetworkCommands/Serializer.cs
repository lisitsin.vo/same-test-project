using System;
using System.IO;
using System.Runtime.Serialization;
using JetBrains.Annotations;
using Newtonsoft.Json;
using TestingCore.Implementations.NetworkCommands.Interfaces;

namespace TestingCore.Implementations.NetworkCommands
{
    public class Serializer : ISerializer
    {
        private readonly JsonSerializer jsonSerializer;

        public Serializer()
        {
            jsonSerializer = new JsonSerializer
                {DateFormatString = DateTimeFormatting.FormatString, DateTimeZoneHandling = DateTimeZoneHandling.Utc};
        }

        public string Accept => "application/json";
        public string ContentType => "application/json";

        public byte[] Serialize<T>(T obj)
        {
            using (var memoryStream = new MemoryStream())
            {
                Serialize(memoryStream, obj, typeof(T));
                return memoryStream.ToArray();
            }
        }

        public T Deserialize<T>([NotNull] byte[] bytes)
        {
            using (var memoryStream = new MemoryStream(bytes))
            {
                return Deserialize<T>(memoryStream);
            }
        }

        private void Serialize([NotNull] Stream stream, object obj, Type objType)
        {
            try
            {
                using (var streamWriter = new StreamWriter(stream))
                {
                    using (var jsonTextWriter = new JsonTextWriter(streamWriter))
                    {
                        jsonSerializer.Serialize(jsonTextWriter, obj, objType);
                        jsonTextWriter.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                var message = $"Fail to serialize object of type {objType}";
                throw new SerializationException(message, e);
            }
        }

        private T Deserialize<T>([NotNull] Stream stream)
        {
            using (var streamReader = new StreamReader(stream))
            {
                if (streamReader.EndOfStream)
                {
                    throw new SerializationException("Fail to deserialize object from empty bytes array.");
                }

                try
                {
                    using (var jsonTextReader = new JsonTextReader(streamReader))
                    {
                        return jsonSerializer.Deserialize<T>(jsonTextReader);
                    }
                }
                catch (Exception e)
                {
                    throw new SerializationException($"Fail to deserialize type {typeof(T)}", e);
                }
            }
        }
    }
}