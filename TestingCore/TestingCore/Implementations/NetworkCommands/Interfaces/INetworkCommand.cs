using System.Threading.Tasks;
using JetBrains.Annotations;

namespace TestingCore.Implementations.NetworkCommands.Interfaces
{
    public interface INetworkCommand
    {
        [NotNull]
        Task<Result<byte[]>> SendAsync();

        [NotNull]
        Task<Result<NetworkResponse>> SendAndGetNetworkResponseAsync();
    }
}