using JetBrains.Annotations;

namespace TestingCore.Implementations.NetworkCommands.Interfaces
{
    public interface ISerializer
    {
        [NotNull] string Accept { get; }
        [NotNull] string ContentType { get; }

        [NotNull]
        byte[] Serialize<T>(T obj);

        T Deserialize<T>(byte[] bytes);
    }
}