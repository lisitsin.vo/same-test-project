using System.Net;
using JetBrains.Annotations;

namespace TestingCore.Implementations.NetworkCommands.Interfaces
{
    public interface INetworkCommandBuilder
    {
        [NotNull]
        INetworkCommandBuilder AddQueryParameter<T>([NotNull] string key, [NotNull] T value);

        [NotNull]
        INetworkCommandBuilder AddHeader([NotNull] string key, [NotNull] string value);

        [NotNull]
        INetworkCommandBuilder SetTimeout(int timeoutInMills);

        [NotNull]
        INetworkCommandBuilder SetBody(byte[] bytes, string contentTypeString);

        INetworkCommandBuilder SetSuccessHttpStatusCode(HttpStatusCode statusCode);

        [NotNull]
        INetworkCommand Build();
    }
}