namespace TestingCore.Implementations.NetworkCommands.Interfaces
{
    public interface INetworkCommandFactory
    {
        INetworkCommandBuilder CreateGet(string path, string accept);
        INetworkCommandBuilder CreatePost(string path, string accept);
    }
}