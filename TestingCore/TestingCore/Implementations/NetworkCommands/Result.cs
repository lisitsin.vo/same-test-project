using System;
using System.Net;

namespace TestingCore.Implementations.NetworkCommands
{
    public class Result<TResult>
    {
        private Exception failureException;
        public bool Success { get; set; }

        public HttpStatusCode HttpStatusCode { get; set; }

        public TResult Value { get; set; }

        public string ErrorMessage { get; set; } = "";

        public TResult EnsuredValue
        {
            get
            {
                if (!Success)
                    throw failureException ?? new Exception(ErrorMessage);
                return Value;
            }
        }

        public static Result<TResult> Ok(HttpStatusCode httpStatus, TResult value)
        {
            return new Result<TResult>
            {
                Success = true,
                HttpStatusCode = httpStatus,
                Value = value
            };
        }

        public static Result<TResult> Failure(HttpStatusCode httpStatus, string errorMessage)
        {
            return new Result<TResult>
            {
                Success = false,
                HttpStatusCode = httpStatus,
                ErrorMessage = errorMessage
            };
        }

        public static Result<TResult> Failure(
            HttpStatusCode httpStatus,
            string errorMessage,
            Exception exception)
        {
            return new Result<TResult>
            {
                HttpStatusCode = httpStatus,
                ErrorMessage = errorMessage,
                failureException = exception
            };
        }
    }
}