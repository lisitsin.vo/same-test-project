using System.Net.Http;
using TestingCore.Implementations.NetworkCommands.Interfaces;

namespace TestingCore.Implementations.NetworkCommands
{
    public class NetworkCommandFactory : INetworkCommandFactory
    {
        private readonly string serverHost;
        private readonly string authToken;
        private readonly string authTokenHeader;

        public NetworkCommandFactory(string serverHost, string authToken, string authTokenHeader)
        {
            this.serverHost = serverHost;
            this.authToken = authToken;
            this.authTokenHeader = authTokenHeader;
        }

        public INetworkCommandBuilder CreateGet(string path, string accept)
        {
            return new NetworkCommandBuilder(HttpMethod.Get, serverHost, path, authToken, accept, authTokenHeader);
        }

        public INetworkCommandBuilder CreatePost(string path, string accept)
        {
            return new NetworkCommandBuilder(HttpMethod.Post, serverHost, path, authToken, accept, authTokenHeader);
        }
    }
}