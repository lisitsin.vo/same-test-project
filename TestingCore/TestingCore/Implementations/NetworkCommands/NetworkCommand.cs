using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TestingCore.Implementations.Extensions;
using TestingCore.Implementations.NetworkCommands.Interfaces;

namespace TestingCore.Implementations.NetworkCommands
{
    public class NetworkCommand : INetworkCommand
    {
        private readonly HttpMethod httpMethod;
        private readonly string serverHost;
        private readonly string path;
        private readonly string authToken;
        private readonly string accept;
        private readonly Dictionary<string, string> queryParams;
        private readonly Dictionary<string, string> headers;
        private readonly string authTokenHeader;
        private readonly HttpStatusCode successHttpStatusCode;

        private readonly byte[] body;
        private readonly int timeoutMills;
        private readonly string contentType;

        public NetworkCommand(
            HttpMethod httpMethod,
            string serverHost,
            string path,
            string authToken,
            string accept,
            string contentType,
            Dictionary<string, string> queryParams,
            byte[] body,
            int timeoutMills,
            Dictionary<string, string> headers,
            string authTokenHeader,
            HttpStatusCode successHttpStatusCode)
        {
            this.httpMethod = httpMethod;
            this.serverHost = serverHost;
            this.path = path;
            this.authToken = authToken;
            this.accept = accept;
            this.contentType = contentType;
            this.queryParams = queryParams;
            this.body = body;
            this.timeoutMills = timeoutMills;
            this.headers = headers;
            this.authTokenHeader = authTokenHeader;
            this.successHttpStatusCode = successHttpStatusCode;
        }

        public async Task<Result<byte[]>> SendAsync()
        {
            return await SendAsync(r => r.Body).ConfigureAwait(false);
        }

        public async Task<Result<NetworkResponse>> SendAndGetNetworkResponseAsync()
        {
            return await SendAsync(r => r).ConfigureAwait(false);
        }

        private async Task<Result<T>> SendAsync<T>(Func<NetworkResponse, T> getResult)
        {
            try
            {
                var response = await GetResponseAsync(CreateWebRequest()).ConfigureAwait(false);
                return response.StatusCode == successHttpStatusCode
                    ? Result<T>.Ok(successHttpStatusCode, getResult(response))
                    : Result<T>.Failure(response.StatusCode, Encoding.UTF8.GetString(response.Body));
            }
            catch (WebException e) when (e.Status == WebExceptionStatus.Timeout)
            {
                return Result<T>.Failure(HttpStatusCode.RequestTimeout, e.Message);
            }
            catch (Exception e)
            {
                return Result<T>.Failure(HttpStatusCode.ServiceUnavailable, e.Message);
            }
        }

        private HttpWebRequest CreateWebRequest()
        {
            var webRequest = (HttpWebRequest) WebRequest.Create(new Uri(BuildUriString()));
            webRequest.Method = httpMethod.ToString().ToUpperInvariant();
            webRequest.Timeout = timeoutMills;
            webRequest.ContentLength = Math.Max(0, webRequest.ContentLength);
            webRequest.ContentType = contentType;
            webRequest.Accept = accept;
            if (!authToken.IsNullOrEmpty())
            {
                if (authTokenHeader.IsNullOrEmpty())
                {
                    throw new ArgumentNullException("Передан authToken, но не заполнено значение Header для запроса");
                }

                webRequest.Headers.Add(authTokenHeader, authToken);
            }

            foreach (var header in headers)
            {
                webRequest.Headers.Add(header.Key, header.Value);
            }

            if (body == null)
            {
                return webRequest;
            }

            webRequest.ContentLength = body.Length;
            return webRequest;
        }

        private string BuildUriString()
        {
            var uriString = $"{serverHost}/{path}";
            if (queryParams != null && queryParams.Count > 0)
            {
                var queryString = string.Join("&",
                    queryParams.Select(p => $"{HttpUtility.UrlEncode(p.Key)}={HttpUtility.UrlEncode(p.Value)}"));
                uriString = $"{uriString}?{queryString}";
            }

            return uriString;
        }

        private async Task<HttpWebResponse> SendRequestAsync(HttpWebRequest webRequest)
        {
            try
            {
                if (body != null)
                {
                    using (var requestStream = await webRequest.GetRequestStreamAsync().ConfigureAwait(false))
                    {
                        await requestStream.WriteAsync(body, 0, body.Length).ConfigureAwait(false);
                    }
                }

                return (HttpWebResponse) await webRequest.GetResponseAsync().ConfigureAwait(false);
            }
            catch (WebException e)
            {
                var webResponse = e.Response as HttpWebResponse;
                if (webResponse == null)
                {
                    throw;
                }

                return webResponse;
            }
        }

        private async Task<NetworkResponse> GetResponseAsync(HttpWebRequest webRequest)
        {
            HttpWebResponse webResponse = null;
            try
            {
                webResponse = await SendRequestAsync(webRequest).ConfigureAwait(false);
                byte[] bytes;
                using (var responseStream = webResponse.GetResponseStream())
                {
                    bytes = await responseStream.ReadToEndAsync().ConfigureAwait(false);
                }

                return new NetworkResponse
                {
                    StatusCode = webResponse.StatusCode,
                    Body = bytes,
                    Headers = webResponse.Headers
                };
            }
            finally
            {
                webResponse?.Close();
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"{httpMethod} {BuildUriString()}");
            sb.AppendLine($"Accept: {accept}");
            if (!string.IsNullOrEmpty(contentType))
            {
                sb.AppendLine($"ContentType: {contentType}");
            }

            if (body != null)
            {
                sb.AppendLine(Encoding.UTF8.GetString(body));
            }

            return sb.ToString();
        }
    }
}