using System.Net;

namespace TestingCore.Implementations.NetworkCommands
{
    public class NetworkResponse
    {
        public byte[] Body { get; set; }

        public WebHeaderCollection Headers { get; set; }

        public HttpStatusCode StatusCode { get; set; }
    }
}