using System;
using JetBrains.Annotations;

namespace TestingCore.Implementations.NetworkCommands
{
    public static class DateTimeFormatting
    {
        public const string FormatString = "yyyy-MM-ddTHH:mm:ss";

        [NotNull]
        public static string Format(this DateTime dateTime) => dateTime.ToString(FormatString);
    }
}