using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using TestingCore.Implementations.NetworkCommands.Interfaces;

namespace TestingCore.Implementations.NetworkCommands
{
    public class NetworkCommandBuilder : INetworkCommandBuilder
    {
        private readonly HttpMethod httpMethod;
        private readonly string serverHost;
        private readonly string path;
        private readonly string authToken;
        private readonly string accept;
        private readonly Dictionary<string, string> queryParams;
        private readonly Dictionary<string, string> headers;
        private byte[] body;
        private int timeoutMills;
        private string contentType;
        private readonly string authTokenHeader;
        private HttpStatusCode successHttpStatusCode = HttpStatusCode.OK;

        public NetworkCommandBuilder(HttpMethod httpMethod, string serverHost, string path, string authToken,
            string accept, string authTokenHeader)
        {
            this.httpMethod = httpMethod;
            this.serverHost = serverHost;
            this.path = path;
            this.authToken = authToken;
            this.accept = accept;
            this.authTokenHeader = authTokenHeader;
            queryParams = new Dictionary<string, string>();
            headers = new Dictionary<string, string>();
            body = null;
        }

        public INetworkCommandBuilder AddQueryParameter<T>(string key, T value)
        {
            if (queryParams.ContainsKey(key))
            {
                throw new ArgumentException($"Key {key} is already added to parameters");
            }

            if (!string.IsNullOrEmpty(key) && value != null)
            {
                queryParams.Add(key, value.ToString());
            }

            return this;
        }

        public INetworkCommandBuilder AddHeader(string key, string value)
        {
            if (this.headers.ContainsKey(key))
            {
                throw new ArgumentException("Key " + key + " is already added to parameters");
            }

            if (!string.IsNullOrEmpty(key) && value != null)
            {
                this.headers.Add(key, value);
            }

            return this;
        }

        public INetworkCommandBuilder SetTimeout(int timeoutInMills)
        {
            timeoutMills = timeoutInMills;
            return this;
        }

        public INetworkCommandBuilder SetBody(byte[] bytes, string contentTypeString)
        {
            contentType = contentTypeString;
            body = bytes;
            return this;
        }

        public INetworkCommandBuilder SetSuccessHttpStatusCode(HttpStatusCode statusCode)
        {
            successHttpStatusCode = statusCode;
            return this;
        }

        public INetworkCommand Build()
        {
            return new NetworkCommand(httpMethod, serverHost, path, authToken, accept, contentType, queryParams, body,
                timeoutMills, headers, authTokenHeader, successHttpStatusCode);
        }
    }
}