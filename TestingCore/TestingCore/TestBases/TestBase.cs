using NUnit.Framework;

namespace TestingCore.TestBases
{
    public abstract class TestBase
    {
        /// <summary>
        /// Метод вызывается один раз перед всеми тестами в одном классе
        /// </summary>
        [OneTimeSetUp]
        protected virtual void FixtureSetUp()
        {
        }

        /// <summary>
        /// Метод вызывается перед каждым тестом
        /// </summary>
        [SetUp]
        protected virtual void SetUp()
        {
        }

        /// <summary>
        /// Метод вызывается после каждого теста
        /// </summary>
        [TearDown]
        protected virtual void TearDown()
        {
        }

        /// <summary>
        /// Метод вызывается после всех тестов в одном классе
        /// </summary>
        [OneTimeTearDown]
        protected virtual void FixtureTearDown()
        {
        }
    }
}